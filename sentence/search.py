from generate import filename
from time import time

"""
定义装饰器函数，计算运行时间
"""
def run_time(func):
    def inner(*args, **kwargs):
        print("开始运行： {}()".format(func.__name__))
        now = time()
        result = func(*args, **kwargs)
        print("运行结束: {}()\n用时{}ms".format(func.__name__, (time() - now) * 1000))

        return result

    return inner

"""
定义一个工具函数，计算一行中cat出现的次数
"""
def count_word(line):
    count = 0;
    for word in line.split(' '):
        if word == 'cat':
            count += 1

    return count

"""
磁盘文件检索
"""
@run_time
def count_word_by_disk():
    count = 0

    with open(filename, encoding='utf_8') as file:
        for line in file:
            count += count_word(line)

    return count

"""
逐行读取文件检索
"""
@run_time
def count_word_by_line():
    count = 0
    file = open(filename, encoding='utf_8')

    while True:
        line = file.readline()
        if line == '': break

        count += count_word(line)

    file.close()

    return count

"""
读取整个文件检索
"""
@run_time
def count_word_by_lines():
    count = 0

    with open(filename, encoding='utf_8') as file:
        for line in file.readlines():
            count += count_word(line)

    return count

print('磁盘检索：“cat”出现了{}次\n'.format(count_word_by_disk()))
print('逐行检索：“cat”出现了{}次\n'.format(count_word_by_line()))
print('全文检索：“cat”出现了{}次\n'.format(count_word_by_lines()))
