from random import seed
from makesentence import sentence

filename = './data.txt'

def generate():
    with open(filename, 'w') as file:
        seed()
        for i in range(100000000):
        #  for i in range(100):
            file.write(' '.join(sentence()) + '\n')

if __name__ == '__main__':
    generate()
