from os import path
import re

# 正则表达式，提取IP地址
ip_regexp = r'(?:\d{1,3}\.){3}\d{1,3}'

def extract():
    # id到邮箱地址的映射
    id_email_dict = {}
    # 记录结果
    result = []

    with open(path.join(path.dirname(__file__), './maillog.txt'), 'r') as file:
        for line in file:
            log = line.split(' ', 1)[1]
            if (log.startswith('starting delivery')):
                start, end = log.split(':', 1)
                id = start.rsplit(' ', 1)[1]
                email = end.rsplit(' ', 1)[1].strip()

                # 放入字典
                id_email_dict[id] = email
            elif (log.startswith('delivery')):
                start, end = log.split(': ', 1)
                id = start.rsplit(' ', 1)[1]
                status, tail = end.split(':', 1)

                # 通过正则查找IP地址
                ip = re.search(ip_regexp, tail)
                if ip: ip = ip.group()

                # 在id_email_dict中查找id对应的邮箱地址
                email = id_email_dict.get(id)

                result.append({
                    'id': id,
                    'status': status,
                    'email': email,
                    'ip': ip
                })

    return result

def get_max_error_email(data):
    email_error_count = {}

    for item in data:
        status = item.get('status')
        email = item.get('email')
        if email is not None and (status == 'failure' or status == 'deferral'):
            email_host = email.split('@')[1]
            if email_error_count.get(email_host) is None:
                email_error_count[email_host] = 0
            email_error_count[email_host] += 1

    # 找到最大错误次数的邮箱
    values = list(email_error_count.values())
    keys = list(email_error_count.keys())
    max_count = max(values)
    max_email = keys[values.index(max_count)]

    return max_email, max_count

if __name__ == "__main__":
    data = extract()

    for item in data:
        print(item)

    print('===========================================================')
    max_email, count = get_max_error_email(data)
    print('错误次数最大的邮箱类型: {}，错误次数: {}'.format(max_email, count))


