import math
import random
from time import time

"""
定义圆柱体类
"""
class Cylinder:
    def __init__(self, radius, height):
        self.radius = radius
        self.height = height

    # 打印圆柱体信息
    def printInfo(self):
        print('圆柱体半径为：{}cm，高为：{}cm，体积为：{}立方cm'.format(self.radius, self.height, self.getVolume()))

    # 计算圆柱体体积
    def getVolume(self):
        return math.pi * self.radius ** 2 * self.height

"""
定义装饰器函数，计算运行时间
"""
def run_time(func):
    def inner(*args, **kwargs):
        print("开始运行： {}()".format(func.__name__))
        now = time()
        result = func(*args, **kwargs)
        print("运行结束: {}()，用时{}ms".format(func.__name__, (time() - now) * 1000))

        return result

    return inner

"""
随机删除list中一个元素，直到全部删除
"""
@run_time
def random_delete(cyliners):
    # 记录随机数产生的次数
    random_count = 0
    while True:
        # 判断是否都已经删除，如果列表中所有的值都为None，则表示删除完毕
        if all(cyliner is None for cyliner in cyliners):
            break;

        # 产生一个随机数当作列表的索引，将指定的值置为None
        index = random.randint(0, len(cyliners) - 1);
        random_count += 1
        if cyliners[index] != None:
            cyliners[index] = None

    print("随机数产生了{}次".format(random_count))

def main():
    cyliners = []
    for x in range(10):
        cyliners.append(Cylinder(x + 1, x + 2))

    for cyliner in cyliners:
        cyliner.printInfo()

    # 删除对象
    random_delete(cyliners)
    print(cyliners)

if __name__ == '__main__':
    main()
