import numpy as np

gamma = 0.9  # 折扣因子  
epsilon = 1e-6  # 收敛阈值  
  
# 状态集：5*5的网格
grid = 5
states = range(grid**2)
# 动作集a1=up a2=right a3=down a4=left a5=stay
actions = ['up', 'right', 'down', 'left', 'stay']

# 从当前状态state，采取动作action，获取下一个状态
# 网格5*5
def get_next_state(state, action):
    if action == 'up' and state // grid > 0:  
        return state - grid  
    elif action == 'down' and state // grid < grid - 1:  
        return state + grid  
    elif action == 'left' and state % grid > 0:  
        return state - 1  
    elif action == 'right' and state % grid < grid - 1:  
        return state + 1  
    else:  
        # 如果无法移动（到达了边界，或者选择停留stay），则停留在当前状态  
        return state
  
# 奖励函数（对于每个状态的即时奖励）
r_boundary = -1
r_forbidden = -1
R = np.zeros(grid**2)
R[6] = R[7] = R[12] = R[16] = R[18] = R[21] = r_forbidden # 禁区
R[17] = 1 # 目标区域

"""
当前状态state，采取动作action，下一个状态的价值
"""
def get_next_value(V, state, action):
    # 获取下一个状态
    next_state = get_next_state(state, action)
    if next_state == state and action != 'stay':
        # 碰壁了
        r = r_boundary
    else:
        r = R[next_state]
                
    # 使用贝尔曼方程计算价值
    return r + gamma * V[next_state]

def policy_evaluation(policy, V):  
    """策略评估"""  
    while True:  
        V_new = V.copy()  
        for s in states:  
            V_new[s] = get_next_value(V, s, policy[s])

        # 检查收敛  
        if np.linalg.norm(V_new - V, ord=np.inf) < epsilon:  
            break  
        
        V = V_new

    return V  

def policy_improvement(policy, V):  
    """策略改进"""  
    for s in states:  
        q_values = [get_next_value(V, s, a) for a in actions]  
        policy[s] = actions[np.argmax(q_values)]   
            
    return policy

"""
R: 奖励矩阵
states: 状态空间
"""
def policy_iteration(R, states):
    # 初始化价值函数为0  
    V = np.zeros_like(R, dtype='float64')
    policy = np.array([np.random.choice(actions) for _ in states], dtype='U5')
    
    # 策略迭代算法  
    # 策略迭代主循环
    while True:
        V_new = policy_evaluation(policy, V)  
        policy = policy_improvement(policy, V_new)  
        # 检查收敛  
        if np.linalg.norm(V_new - V, ord=np.inf) < epsilon:  
            break

        V = V_new

    return policy

print(policy_iteration(R, states))