import numpy as np

gamma = 0.9  # 折扣因子  
epsilon = 1e-6  # 收敛阈值  
  
# 状态集：5*5的网格
grid = 5
states = range(grid**2)
# 动作集a1=up a2=right a3=down a4=left a5=stay
actions = ['up', 'right', 'down', 'left', 'stay']

# 从当前状态state，采取动作action，获取下一个状态
# 网格5*5
def get_next_state(state, action):
    if action == 'up' and state // grid > 0:  
        return state - grid  
    elif action == 'down' and state // grid < grid - 1:  
        return state + grid  
    elif action == 'left' and state % grid > 0:  
        return state - 1  
    elif action == 'right' and state % grid < grid - 1:  
        return state + 1  
    else:  
        # 如果无法移动（到达了边界，或者选择停留stay），则停留在当前状态  
        return state
  
# 奖励函数（对于每个状态的即时奖励）
r_boundary = -1
r_forbidden = -10
R = np.zeros(grid**2)
R[6] = R[7] = R[12] = R[16] = R[18] = R[21] = r_forbidden # 禁区
R[17] = 1 # 目标区域

"""
R: 奖励矩阵
gamma: 折扣因子
states: 状态空间
epsilon: 收敛阈值
"""
def value_iteration(R, states):
    # 初始化价值函数为0  
    V = np.zeros_like(R, dtype='float64')
    policy = np.array([np.random.choice(actions) for _ in states], dtype='U5')
    
    # 值迭代算法  
    while True:  
        V_new = V.copy()
        # 对于每个状态
        for s in states:
            # 对于每个动作
            q_values = []
            for a in actions:
                # 获取下一个状态
                next_state = get_next_state(s, a)
                if next_state == s and a != 'stay':
                    # 碰壁了
                    r = r_boundary
                else:
                    r = R[next_state]
                
                # 使用贝尔曼方程更新价值
                q_values.append(r + gamma * V[next_state])

            V_new[s] = max(q_values)
            policy[s] = actions[np.argmax(q_values)]
        
        # 检查收敛  
        if np.linalg.norm(V_new - V, ord=np.inf) < epsilon:  
            break  
        
        V = V_new

    return V, policy

print(value_iteration(R, states))