import random

chars = [x for x in 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ']
marks = [x for x in '~!@#$%^&*()_+-={}|:"[]\\;\',./<>?']
numbers = [x for x in '0123456789']

# 生成随机密码
def generate(length: int):
    if (length < 3):
        raise Exception('最小密码长度为3')

    # 保存密码字符的列表
    password = []
    # 添加一个特殊符号
    password.append(get_random_char(marks))
    # 添加一个数字
    password.append(get_random_char(numbers))
    # 剩下的长度全部添加字母
    for i in range(2, length):
        password.append(get_random_char(chars))

    # 打乱顺序
    random.shuffle(password)

    # 最后返回密码字符串
    return ''.join(password)

# 在列表中随机获取一项
def get_random_char(list):
    return list[random.randint(0, len(list) - 1)]

print(generate(3))
print(generate(6))
print(generate(10))
