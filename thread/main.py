from threading import Thread, Lock

result = []
lock = Lock()

def squ(startIndex, numbers):
    # 计算每个数的平方
    numbers = [x ** 2 for x in numbers]

    # 获取锁，将结果写入全局变量result，然后释放锁
    lock.acquire()
    global result
    result[startIndex:len(numbers)] = numbers
    lock.release()


def main():
    numbers = [x for x in range(1, 10001)]
    # 每个线程计算的数量
    count_per_thread = int(len(numbers) / 10)
    threads = []

    for i in range(10):
        # 每个线程计算numbers中的一个切片，长度100，根据切片在numbers中索引，在squ函数中，将计算结果放入到对应的位置
        t = Thread(target=squ, args=(i * count_per_thread, numbers[i * count_per_thread:(i + 1) * count_per_thread]))
        t.start()
        threads.append(t)

    # 等待所有线程结束
    for t in threads:
        t.join()

    print(result)


if __name__ == '__main__':
    main()
