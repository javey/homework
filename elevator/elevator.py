from Floor.floor import calculate_actual_floor

# 楼层范围
range = [-4, 20]
# 不存在的楼层序列，必须在楼层范围内，并且升序排列
vacancies = [0, 14, 18]
# 每层楼的高度（米）
height = 3

class RangeError(Exception):
    pass
class VacancyError(Exception):
    pass

while True:
    try:
        floor = int(input("请输入楼层："))

        if (floor < range[0] or floor > range[1]):
            raise RangeError
        if (floor in vacancies):
            raise VacancyError

        # 计算实际楼层编号
        actual_floor = calculate_actual_floor(floor, vacancies)

        print("电梯上升高度：", actual_floor * height, "米")

        break
    except ValueError:
        print("只能输入数字")
        continue
    except RangeError:
        print("楼层编号超出范围：", range)
        continue
    except VacancyError:
        print("楼层不存在")
        continue
