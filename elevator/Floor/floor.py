import unittest

def calculate_actual_floor(floor, vacancies):
    # 当前输入的楼层编号，在不存在的楼层编号中依次寻找，直到找到比当前输入值大的数，或者到达序列末尾，即停止
    # 此时，索引号就代表当前楼层之前有多少个不存在的楼层，减去相应的不存在的楼层数量就是实际楼层
    count = 0
    for vacancy in vacancies:
        if (floor < vacancy):
            break;
        count += 1

    return floor - count

class Test(unittest.TestCase):
    def test_get_actual_floor(self):
        self.assertEqual(calculate_actual_floor(9, [8, 10]), 8)
    
    def test_get_actual_floor_from_empty(self):
        self.assertEqual(calculate_actual_floor(13, []), 13)

    def test_get_actual_floor_exceed_range(self):
        self.assertEqual(calculate_actual_floor(13, [8, 10]), 11)

if __name__ == '__main__':
    unittest.main()